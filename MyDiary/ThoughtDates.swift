//
//  ThoughtDates.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 9/27/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import Foundation

struct ThoughtDate {
    var date: String!
    var thoughts: [Thought]?
    
    init(date: String) {
        self.date = date
    }
}

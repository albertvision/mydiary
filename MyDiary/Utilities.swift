//
//  Utilities.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 9/25/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication
import CryptoSwift

class Utilities {
    
    var cipher: Cipher?;
    
    static func alert(vc: UIViewController, title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func substr(string: String, toIndex: Int) -> String {
        var toIndex = toIndex
        
        if string.characters.count < toIndex {
            toIndex = string.characters.count
        }
        
        return string[string.startIndex..<string.index(string.startIndex, offsetBy: toIndex)]
    }
    
    static func makeAuth(callback: @escaping (_ error: NSError?) -> Void) {
        let authContext = LAContext()
        var authError: NSError?
        
        if !authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            callback(authError)
        }
        
        authContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "To access your notes, scan your finger") { (isSuccessfulC, error) in
            callback(error as NSError?)
        }
    }
    
    static func groupThoughtsByDates(thoughts: [Thought2]) -> [String: [Thought2]] {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        var byDates = [String: [Thought2]]()
        
        for thought in thoughts {
            let date = formatter.string(from: thought.createdAt)
            
            if byDates[date] == nil {
                byDates[date] = [Thought2]()
            }
            
            byDates[date]?.append(thought)
        }
        
        return byDates
    }
    
    static func encrypt(str: String) -> String? {
        if let cipher = UserDefaults().string(forKey: "pkey") {
            do {
                let aes = try AES(key: cipher.utf8.map({$0}))
                
                return try aes.encrypt(str.utf8.map({$0})).toBase64()
            } catch {
                
            }
        }
        
        return nil
    }
    
    func getCipher() -> Cipher? {
        if self.cipher == nil {
            if let cipher = UserDefaults().string(forKey: "pkey") {
                do {
                    self.cipher = try AES(key: cipher.utf8.map({$0}))
                } catch let error {
                    print("Cipher cannot be used: \(error.localizedDescription)")
                }
                
            }
        }
        
        return self.cipher
    }
}

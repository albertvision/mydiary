//
//  ListTableViewController.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 9/25/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import UIKit
import CoreData
import LocalAuthentication

import Firebase
import FirebaseDatabase

class ListTableViewController: UITableViewController {
    
    let cellReusableId: String = "thoughtCell"
    let coreDatahelper = CoreDataHelper()
    let formatter = DateFormatter()
    
    var thoughts = [String: [Thought2]]()
    var dates = [String]()
    
    var thoughts2 = [Thought2]()
    
    var db: FIRDatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        formatter.dateFormat = "dd.MM.yyyy, HH:mm"
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "newThoughtVc")
//        self.show(vc!, sender: self)
        
        self.refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged )
        
        self.db = FIRDatabase.database().reference()
        
        let sectionFormatter = DateFormatter()
        sectionFormatter.dateFormat = "dd.MM.yyyy"
        
        self.db.child("thoughts").queryOrdered(byChild: "createdAt").observe(.childAdded, with: { (snapshot) in
            let thought = Thought2(snap: snapshot)
            let date = sectionFormatter.string(from: thought.createdAt)
            
            var dateIndex = self.dates.index(of: date)
            
            if dateIndex == nil {
                self.thoughts[date] = [Thought2]()
                self.dates.insert(date, at: 0)
                dateIndex = 0
            }
            
            self.thoughts[date]!.insert(thought, at: 0)
            self.tableView.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    func refreshList() {
        print("zdr bebce")
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.dates[section]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.dates.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.thoughts.count == 0 {
            return 0
        }
        
        return self.thoughts[self.dates[section]]!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellReusableId, for: indexPath)
//        let thought = Thought2 self.thoughts2[indexPath.row]
//        let thought = self.thoughts2[indexPath.row]
        
//        print(thought)
        
        let thought = self.thoughts[(self.dates[indexPath.section])]![indexPath.row]

        cell.textLabel?.text = Utilities.substr(string: thought.body, toIndex: 50)
        cell.detailTextLabel?.text = formatter.string(from: thought.createdAt)

        return cell
    }
    
    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openThoughtDetails" {
            let vc = segue.destination as! ThoughtViewController
            let index = self.tableView.indexPath(for: sender as! UITableViewCell)!

            vc.thought = thoughts[self.dates[index.section]]![index.row]
        }
    }

}

//
//  Security.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 10/4/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import Foundation
import CryptoSwift

enum SecurityError: Error {
    case cipherNotFound
    case cipherCannotBeUsed(err: Error?)
    case notEncrytped(err: Error?)
    case notDecrypted(err: Error?)
}

class Security {
    private var cipher: Cipher!
    
    init() throws {
        guard let rawCipher = UserDefaults().string(forKey: KeyName.cipher) else {
            throw SecurityError.cipherNotFound
        }
        
        do {
            self.cipher = try AES(key: rawCipher.utf8.map({$0}))
        } catch let error {
            throw SecurityError.cipherCannotBeUsed(err: error)
        }
    }
    
    func encrypt(str: String) throws -> String {
        do {
            return try str.encrypt(cipher: self.cipher)
        } catch let error {
            throw SecurityError.notEncrytped(err: error)
        }
    }
    
    func decrypt(hexString: String) throws -> String? {
        do {
            return String(bytes: try Array<UInt8>(hex: "0x" + hexString).decrypt(cipher: self.cipher), encoding: .utf8)
        } catch let error {
            throw SecurityError.notDecrypted(err: error)
        }
    }
}

//
//  DataCoreHelper.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 9/25/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataHelper {
    init() {
        let container = NSPersistentContainer.init(name: "MyDiary")
        container.loadPersistentStores { (storeDescription, error) in
            if let error = error {
                print("DB-Err: \(error)")
                
                return
            }
        }
    }
    
    func getContext() -> NSManagedObjectContext {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    
    func saveThought(text: String) -> Bool {
        let context = self.getContext()
        let entity = Thought(context: context)
        entity.createdAt = NSDate()
        entity.text = text
        
        do {
            try context.save()
            
            return true
        } catch let error {
            print("Insert error: \(error)")
            
            return false
        }
    }
    
    func getThoughts() -> [Thought]? {
        let request: NSFetchRequest<Thought> = Thought.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "createdAt", ascending: false)]
        
        do {
            let res = try self.getContext().fetch(request)
            
            return res as [Thought]
        } catch let error {
            print("Fetching error: \(error)")
            
            return nil
        }
        
    }
}

//
//  AuthViewController.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 9/28/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class AuthViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    var originalFrameSize: CGSize!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        do {
//            try FIRAuth.auth()?.signOut()
//        } catch {
//            print("cant logout")
//        }
        
        if FIRAuth.auth()?.currentUser != nil {
            self.openNextController()
        }
        
        self.originalFrameSize = self.view.frame.size
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        
        self.emailField.becomeFirstResponder()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapOnBg(_ sender: UITapGestureRecognizer) {
        self.emailField.resignFirstResponder()
        self.passwordField.resignFirstResponder()
    }
    func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo as? [AnyHashable : AnyObject]
        let keyboardHeight: CGFloat = userInfo![UIKeyboardFrameBeginUserInfoKey]!.cgRectValue.size.height
        let offsetHeight: CGFloat = userInfo![UIKeyboardFrameEndUserInfoKey]!.cgRectValue.size.height
        
        var heightDiff = keyboardHeight;
        
        if(keyboardHeight != offsetHeight) {
            heightDiff -= (keyboardHeight - offsetHeight)
        }
        
        UIView.animate(withDuration: 0.1) {
            self.view.frame.size.height = self.originalFrameSize.height - heightDiff
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1) {
            self.view.frame.size.height = self.originalFrameSize.height
        }
    }
    @IBAction func goToNextField(_ sender: AnyObject) {
//        self.emailField.resignFirstResponder()
        self.passwordField.becomeFirstResponder()
    }
    
    @IBAction func loginClicked(_ sender: AnyObject) {
        
            FIRAuth.auth()?.signIn(withEmail: self.emailField.text!, password: self.passwordField.text!, completion: { (user, error) in
                if let error = error {
                    Utilities.alert(vc: self, title: "Error", message: error.localizedDescription)
                    
                    return
                }
                
                let cipher = self.passwordField.text?.md5()
                
                UserDefaults().set(cipher, forKey: "pkey")
                
                self.openNextController()
            })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func openNextController() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "authRequiredVc")
        DispatchQueue.main.async {
            self.present(vc!, animated: true) {
                print("is it opened")
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

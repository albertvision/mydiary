//
//  NewThoughtViewController.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 9/25/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import UIKit
import Firebase

import FirebaseDatabase
import CryptoSwift

class NewThoughtViewController: UIViewController {

    @IBOutlet weak var textField: UITextView!
    
    var originalTextBottom: CGFloat!
    var isKeyboardOpened = false
    
    var db: FIRDatabaseReference!
    private var dbHandler: FIRDatabaseHandle!
    
    var security: Security!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.originalTextBottom = self.textField.contentInset.bottom
        
        do {
            self.security = try Security()
        } catch {
            // @todo
        }
        
        // Fix keyboard-scroll issue
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        
        self.db = FIRDatabase.database().reference()
        
        self.textField.text = ""
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.textField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveClicked(_ sender: AnyObject) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        do {
            self.db.child("thoughts").childByAutoId().setValue([
                "body": try self.security.encrypt(str: self.textField.text),
                "createdAt": formatter.string(from: Date()) as NSString
            ], withCompletionBlock: { (error, dbRef) in
                if error != nil {
                    self.showCannotBeAddedMsg(error: error)
                    return
                }
                
                Utilities.alert(vc: self, title: nil, message: "Thought saved successfully!")
                self.textField.text = ""
                    
            })
        } catch {
            self.showCannotBeAddedMsg(error: nil)
        }
        
        
    }
    
    func showCannotBeAddedMsg(error: Error?) {
        DispatchQueue.main.async {
            Utilities.alert(vc: self, title: "Thought cannot be added.", message: error?.localizedDescription)
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo as? [AnyHashable : AnyObject]
        let keyboardHeight: CGFloat = userInfo![UIKeyboardFrameBeginUserInfoKey]!.cgRectValue.size.height
        let offsetHeight: CGFloat = userInfo![UIKeyboardFrameEndUserInfoKey]!.cgRectValue.size.height
        
        if(keyboardHeight == offsetHeight) {
            self.textField.contentInset.bottom += keyboardHeight
            self.textField.scrollIndicatorInsets.bottom += keyboardHeight
        } else {
            let diff = offsetHeight - keyboardHeight
            
            self.textField.contentInset.bottom += diff
            self.textField.scrollIndicatorInsets.bottom += diff
        }
        
        self.isKeyboardOpened = true
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if !isKeyboardOpened {
            return
        }
        
        let userInfo = notification.userInfo as? [AnyHashable : AnyObject]
        let keyboardHeight: CGFloat = userInfo![UIKeyboardFrameBeginUserInfoKey]!.cgRectValue.size.height
        let offsetHeight: CGFloat = userInfo![UIKeyboardFrameEndUserInfoKey]!.cgRectValue.size.height
        
        self.textField.contentInset.bottom -= keyboardHeight
        self.textField.scrollIndicatorInsets.bottom -= keyboardHeight
        
        self.isKeyboardOpened = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

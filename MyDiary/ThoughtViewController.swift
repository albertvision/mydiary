//
//  ThoughtViewController.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 9/26/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import UIKit

class ThoughtViewController: UIViewController {

    var thought: Thought2!
    
    @IBOutlet weak var bodyTextController: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy, HH:mm"

        bodyTextController.text = thought.body
        self.title = formatter.string(from: thought.createdAt as! Date)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  AuthRequiredViewController.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 9/25/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class AuthRequiredViewController: UIViewController {

    var reason: String?
    var navCtrl: UINavigationController!
    
    @IBOutlet weak var reasonLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if reason != nil {
            reasonLabel.text = reason
        }
        
        self.navCtrl = self.storyboard?.instantiateViewController(withIdentifier: "mainNavigationController") as! UINavigationController
        
        if FIRAuth.auth()?.currentUser == nil {
            let authVc = self.storyboard!.instantiateViewController(withIdentifier: "authVc")
            
            DispatchQueue.main.async {
                self.show(authVc, sender: self)
            }
            
        } else {
            self.makeAuth()
        }
        
//        self.present(self.navCtrl, animated: true, completion: nil)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeAuth() {
        Utilities.makeAuth { (error) in
            if error != nil {
                Utilities.alert(vc: self, title: "Authorisation error", message: error!.localizedDescription)
                return
            }
            DispatchQueue.main.async {
                self.present(self.navCtrl, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func tryAgainClicked(_ sender: AnyObject) {
        self.makeAuth()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

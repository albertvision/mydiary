//
//  Thought.swift
//  MyDiary
//
//  Created by Yasen Georgiev on 10/2/16.
//  Copyright © 2016 Yasen Georgiev. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class Thought2 {
    
    public var body: String!
    public var createdAt: Date!
    
    init(params: Dictionary<String, String>) {
        self.initialise(params: params)
    }
    
    init(snap: FIRDataSnapshot) {
        self.initialise(params: snap.value as! Dictionary<String, String>)
    }
    
    private func initialise(params: Dictionary<String, String>) {
        do {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
            
            self.body = try Security().decrypt(hexString: params["body"]!)
            self.createdAt = formatter.date(from: params["createdAt"]!)
        } catch {
            print("cannot be initted")
        }
        
    }
}
